// Functions for managing a 2d array of variable length

#ifndef _PILES_H_
#define _PILES_H_

#include <stdint.h>

// Define the piles structure
typedef struct {
	int** piles;
	int* pilesLen;
	int len;
} piles;

// Function prototypes
void pilesInit(piles* p);
void newPile(piles* p);
void manyNewPiles(piles* p, int newPiles);
void addToPile(piles* p, int pile, int value);
void clearPile(piles* p);
void deletePile(piles* p);
uint64_t pilesToArray(piles* pile, int* a);
uint64_t mergePilesToArray(piles* pile, int* a);
void printPiles(piles* p);

#endif // _PILES_H_