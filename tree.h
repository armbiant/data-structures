// Functions for managing binary arrays

#ifndef _TREE_H_
#define _TREE_H_

#include <stdint.h>

// Define the tree structure
typedef struct node node;
struct node {
	int val;
	node* left,* right; 
};

// Function prototypes
void makeTree(node **n, int *p, int len, int index);
void printTree(node *n, int level, int len);
int treeInOrder(node *n, int *p, int level);
uint64_t bstInsert(node *n, int *p, int index);
void treeDelete(node *n, int level);

node *splay(node *root, int key, uint64_t *c);
node *splayInsert(node *root, int key, uint64_t *c);
node* newNode(int newKey);
node *rotateRight(node *x);
node *rotateLeft(node *x);
node *splay(node *root, int newKey, uint64_t *c);

#endif // _TREE_H_