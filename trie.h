#ifndef _TRIE_H_
#define _TRIE_H_

// Define the trie structure
typedef struct trie trie;
struct trie {
	int value;		// Value held in the trie (typically a counter)
	char size;		// Size (base) of this trie structure. Must be equal in all children
	char offset;	// Offset used when trie values don't start at zero
	trie** trie;	// Array of trie pointers to point to its children
};

// Trie-related functions

void trieInit (trie* t, char size, char offset);
void printTrie (trie* t);
int trieInsert (trie* t, int value, int digits);
void trieToArray (trie* t, int* p, int maxDepth);
void trieClear (trie* t);
void trieDelete (trie* t);

#endif // _TRIE_H_