/* ==========================================================================
	 _____
	|_   _| __ ___  ___   ___
	  | || '__/ _ \/ _ \ / __|
	  | || | |  __/  __/| (__
	  |_||_|  \___|\___(_)___|

	This is a reusable data structure for binary trees. Binary trees consist
	of nodes tht contain a value (in this case, an integer) and two pointers
	to its child nodes.

	The child node on the left must be lower or equal to its parent, and the
	right child must be greater than or equal to its parent.

	This file also contains splays, which are very similar to binary trees,
	but they insert new items at the top rather than the bottom. This makes
	recent items much quicker to access.

 ========================================================================== */

#include "tree.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// Makes a tree from an array (I think)
void makeTree(node **n, int *p, int len, int index) {
	// Make sure we're not at the end of the array
	if (index < len) {
		printf("Creating node %i:%i\n", index, p[index]);
		// Allocate memory for the node
		*n = malloc(sizeof(node));
		// Set the value of the node
		(*n)->val = p[index];
		// Create new nodes for left and right
		makeTree(&(*n)->left, p, len, 2 * index + 1);
		makeTree(&(*n)->right, p, len, 2 * index + 2);
	} else {
		*n = NULL;
	}
	return;
}

// Print a tree. Only works heap-style.
void printTree(node *n, int level, int len) {
	static int* d;
	int i, j, k;
	if (level == 0){
		d = calloc(len*len,sizeof(int));
	}
	if (n) {
		for (i=0;i<len;i++) {
			if (d[len*level+i] == 0) {
				d[len*level+i] = n->val;
				break;
			}
		}
		
		printTree(n->left, level+1, len);
		printTree(n->right, level+1, len);
	}
	// Finishing, let's print some stuff
	if (level == 0){
		// Loop through rows in the 2D array
		for (i=0;i<len;i++) {
			j = 0;
			// Indent a bit on alternating rows
			if (i % 2) {
				printf("  ");
			}
			// Loop through cells
			while(d[len*i+j] && j < len) {
			// Add some indentation for the row
			for (k=0; k<log2(len)-(i+1); k++) {
				printf("     ");
			}
				printf("%i    ",d[len*i+(j++)]);
			}
			printf("\n");
		}
	}
	free(d);
}

// Traverse the tree inOrder and place values in an array
int treeInOrder(node *n, int *p, int level) {
	static int index = 0;
	uint64_t c = 0;
	// Has a left child?
	if (n->left != NULL) {
		// Load left child node
		c += treeInOrder(n->left, p, level+1);
	}

	// Pass the value to the p array
	p[index++] = n->val;

	// Has unprinted right child?
	if (n->right != NULL && n->right->val >= p[index-1]) {
		//printf("Moving right...\n");
		c += treeInOrder(n->right, p, level+1);
	} else {
		// Return to parent
		if (level == 0) {
			index = 0;
		}
		return c;
	}
	
	// Already Printed?
	if (n->right->val <= p[index-1]) {
		// Load parent
		if (level == 0) {
			index = 0;
		}
		return c;
	} else {
		// Print node
		p[index++] = n->val;
	}
}

// Insert an item into the binary tree
uint64_t bstInsert(node *n, int *p, int index) {
	uint64_t c = 1;
	// Check to see if we're at the end of the tree
	if (p[index] <= n->val) {
		// Move leftwards
		if (n->left == NULL) {
			// End of line, create new node
			n->left = (node*)malloc(sizeof(node));
			n->left->val = p[index];
			n->left->left = n->left->right = NULL;
		} else {
			// Carry on with search
			c += bstInsert(n->left, p, index);
		}
	} else if (p[index] > n->val) {
		// Move rightwards
		if (n->right == NULL) {
			// End of line, create new node
			n->right = (node*)malloc(sizeof(node));
			n->right->val = p[index];
			n->right->left = n->right->right = NULL;
		} else {
			// Carry on with search
			c += bstInsert(n->right, p, index);
		}
	}
	return c;
}

// Free the memory of all nodes in a tree
void treeDelete(node *n, int level) {
	// Do nothing if node does not exist
	if (n == NULL) {return;}

	// Continue recursively along each branch
	treeDelete(n->left, level+1);
	treeDelete(n->right, level+1);

	// Delete node
	free(n);
	return;
}


/* =======================================================================
	 ____
	/ ___| _ __ | | __ _ _   _
	\___ \| '_ \| |/ _` | | | |
	 ___) | |_) | | (_| | |_| |
	|____/| .__/|_|\__,_|\__, |
	      |_|            |___/

	These are functions relating specifically to splays.

========================================================================== */

// Function to create a new node
node* newNode(int newKey) {
	node* n = (struct node*)malloc(sizeof(node));
	n->val = newKey;
	n->left = n->right = NULL;
	return (n);
}

// Rotating function
node *rotateRight(node *x) {
	node *y = x->left;
	x->left = y->right;
	y->right = x;
	return y;
}

node *rotateLeft(node *x) {
	node *y = x->right;
	x->right = y->left;
	y->left = x;
	return y;
}

// Perform a splay on the tree. This will modify the tree and return the new root.
node *splay(node *root, int newKey, uint64_t *c) {

	// Check to see if root is either NULL or already the value we want
	if (root == NULL || newKey == root->val) {
		return root;
	}

	*c += 1;
	if (newKey < root->val) {
		// Check out left branch
		// Check if left branch is empty
		if (root->left == NULL) {
			// Nothing to see here
			return root;
		}

		// Do some zigging and zagging
		*c += 1;
		if (newKey < root->left->val) {
			// Zig-Zig (left/left)
			root->left->left = splay(root->left->left, newKey, c);
			root = rotateRight(root);
		} else if (newKey > root->left->val) {
			// Zig-zag (left/right)
			root->left->right = splay(root->left->right, newKey, c);
			if (root->left->right != NULL) {
				root->left = rotateLeft(root->left);
			}
		}

		// Do a second rotation
		if (root->left == NULL) {
			return root;
		} else {
			return rotateRight(root);
		}

	} else {
		// Check out right branch
		// Check if right branch is empty
		if (root->right == NULL) {
			// Nothing to see here
			return root;
		}

		// Do some zigging and zagging
		*c += 1;
		if (newKey < root->right->val) {
			// Zag-Zig (right/left)
			root->right->left = splay(root->right->left, newKey, c);
			if (root->right->left != NULL) {
				root->right = rotateRight(root->right);
			}
		} else if (newKey > root->right->val) {
			// Zag-zag (right/right)
			root->right->right = splay(root->right->right, newKey, c);
			root = rotateLeft(root);
		}

		// Do a second rotation
		if (root->right == NULL) {
			return root;
		} else {
			return rotateLeft(root);
		}
	}
}

// Insert a new node into the splay
node *splayInsert(node *root, int newKey, uint64_t *c) {
	// Check for null
	if (root == NULL) {
		return newNode(newKey);
	}
	// Bring the closest leaf node to root
	root = splay(root, newKey, c);

	// Let's create a new node
	node *n = newNode(newKey);

	// Compare the newKey to the root and move it to either side
	if (newKey < root->val) {
		n->right = root;
		n->left = root->left;
		root->left = NULL;
	} else {
		n->left = root;
		n->right = root->right;
		root->right = NULL;
	}
 
	return n;
}