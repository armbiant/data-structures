/* ====================================================================================
	 ____            _            
	| __ )  __ _ ___(_) ___   ___ 
	|  _ \ / _` / __| |/ __| / __|
	| |_) | (_| \__ | | (__ | (__ 
	|____/ \__,_|___|_|\___(_\___|

	The placeholder for other files that might use this functionality.

 ==================================================================================== */

#include <stdio.h>

void swap(int* a, int* b) {
	*a ^= *b ^= *a ^=*b;
}

void insert(int* a, int b) {
	*a = b;
}

char isLessThan(int a, int b) {
	printf("Comparing %i < %i\n", a, b);
	return (a < b);
}

char isGreaterThan(int a, int b) {
	printf("Comparing %i > %i\n", a, b);
	return (a > b);
}