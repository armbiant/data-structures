#ifndef _BASIC_DATA_STRUCT_FUNCS_H_
#define _BASIC_DATA_STRUCT_FUNCS_H_

// Function prototypes
void swap(int* a, int* b);
void insert(int* a, int b);
char isLessThan(int a, int b);
char isGreaterThan(int a, int b);

#endif // _BASIC_DATA_STRUCT_FUNCS_H_