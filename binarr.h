// Functions for managing binary arrays

#ifndef _BINARR_H_
#define _BINARR_H_
#define BINARR_0 0x00
#define BINARR_1 0x01

// Define the binarr structure
typedef struct {
	unsigned char* p;
	int len;
} binarr;

// Function prototypes
void binarrInit (binarr* b, int len);

void binarrSet (binarr* b, int pos, char val);

void binarrFlip (binarr* b, int pos);

int binarrGet (binarr* b, int pos);

void binarrPrint (binarr* b);
void binarr2dPrint (binarr* b, int maxLen);

void binarrShift (binarr* b, int shifts);

void binarrFree (binarr* b);

#endif // BINARR_H_