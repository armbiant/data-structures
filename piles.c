/* ==========================================================================
	 ____  _ _
	|  _ \(_) | ___  ___   ___ 
	| |_) | | |/ _ \/ __| / __|
	|  __/| | |  __/\__ \| (__ 
	|_|   |_|_|\___||___(_)___|

	Piles are a data structure that work like a 2D array, except that instead
	of having fixed lenths in both directions, the size of the 2nd dimension
	can vary from pile to pile.

 ========================================================================== */

#include "piles.h"
#include "basic.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// A function to print the current status of the piles on the board
void printPiles(piles* p) {
	int i, j;
	printf("Piles: %i\n", p->len);
	// Loop through piles
	for (i=0; i<p->len; i++) {
		printf("%i\t[",p->pilesLen[i]);
		// Loop through elements
		for (j=0; j<p->pilesLen[i]; j++) {
			if (j == p->pilesLen[i]-1) {
				printf("%i",p->piles[i][j]);
			} else {
				printf("%i, ",p->piles[i][j]);
			}
		}
		printf("]\n");
	}
}

// Initialise all values in the pile to zero
void pilesInit(piles* p) {
	p->len = 0;
	p->piles = NULL;
	p->pilesLen = NULL;
}

// Creates a new pile at the end
void newPile(piles* p) {
	// Check to see if there are any existing piles
	p->len++;
	p->piles = realloc(p->piles, p->len * sizeof(int*));
	p->piles[p->len-1] = NULL;
	// Add a new item to length counter and make it zero
	p->pilesLen = realloc(p->pilesLen, p->len * sizeof(int));
	p->pilesLen[p->len-1] = 0;
}

// Creates many new files
void manyNewPiles(piles* p, int newPiles) {
	for (int i=0; i<newPiles; i++) {
		newPile(p);
	}
}

// Appends a value to a particular pile's index
void addToPile(piles* p, int pile, int value) {
	if (pile >= p->len || pile < 0) {
		newPile(p);
		pile = p->len-1;
	}
	p->pilesLen[pile]++;
	p->piles[pile] = realloc(p->piles[pile], p->pilesLen[pile] * sizeof(int));
	p->piles[pile][p->pilesLen[pile]-1] = value;

}

// Frees the memory of all piles
void clearPile(piles* p) {
	for (int i=0; i<p->len; i++) {
		free(p->piles[i]);
		p->piles[i] = NULL;
	}
	free(p->pilesLen);
	p->pilesLen = NULL;
	free(p->piles);
	p->piles = NULL;
}

// Clear the pile and the pile pointer as well
void deletePile(piles* p) {
	clearPile(p);
	free(p);
	p = NULL;
}

// Insert piles into an array in a linear fashion
// Fast, but requires piles to be sorted internally and between each other
uint64_t pilesToArray(piles* pile, int* a) {
	int k = 0;		// Array position counter
	int i, j;		// Loop iterators
	uint64_t c = 0;	// Comparison counter
	// Loop through piles
	for (i=0; i<pile->len; i++) {
		// Loop through each element in the pile
		for (j=0; j<pile->pilesLen[i]; j++, c++) {
			// Insert it into the array
			insert(&a[k++], pile->piles[i][j]);
		}
	}
	return c;
}

// Puts piles into an array using Merge Sort
// Slower, but it piles only need to be sorted internally
uint64_t mergePilesToArray(piles* pile, int* a) {
	uint64_t c = 0;		// Time complexity counter
	int i, j, it;		// Loop iterator
	int i1, i2;			// Pile temp index
	int *temp;			// Temporary array to hold the merging lists
	piles p;

	// Make a copy of the pile
	memcpy(&p, pile, sizeof(piles));
	// Copy the pile lengths
	p.pilesLen = malloc(p.len * sizeof(int));
	memcpy(p.pilesLen, pile->pilesLen, p.len * sizeof(int));
	// Copy the piles
	p.piles = malloc(p.len * sizeof(int*));
	for (i=0; i<p.len; i++) {
		p.piles[i] = malloc(p.pilesLen[i] * sizeof(int));
		memcpy(p.piles[i], pile->piles[i], p.pilesLen[i] * sizeof(int));
	}

	// As long as there is more than one pile
	while(p.len > 1) {
		//printf("Templen: %i\n", p.len);
		// Take the first and last pile
		for(i=0; i<p.len/2; i++) {
			i1 = i2 = 0;
			temp = calloc((p.pilesLen[i] + p.pilesLen[p.len-1-i]), sizeof(int));
			// Loop through each element on each list
			it = 0; // Index for the temp array
			while (i1 < p.pilesLen[i] && i2 < p.pilesLen[p.len-1-i]) {
				c++;
				if (p.piles[i][i1] < p.piles[p.len-1-i][i2]) {
					// Insert from a1
					insert(&temp[it], p.piles[i][i1]);
					i1++;
					// Check for end
					if (i1 >= p.pilesLen[i]) {
						// Fill in the rest of the array with a2
						for (i2; i2<p.pilesLen[p.len-1-i]; i2++) {
							insert(&temp[++it], p.piles[p.len-1-i][i2]);
						}
					}
				} else {
					// Insert from a2
					insert(&temp[it], p.piles[p.len-1-i][i2]);
					i2++;
					// Check for end
					if (i2 >= p.pilesLen[p.len-1-i]) {
						// Fill in the rest of the array with a1
						for (i1; i1<p.pilesLen[i]; i1++) {
							insert(&temp[++it], p.piles[i][i1]);
						}
					}
				}
				it++;
			}
			// Clean things up
			// Increase the size of a1 to accomodate the merge
			p.pilesLen[i] += p.pilesLen[p.len-1-i];
			p.piles[i] = realloc(p.piles[i],(p.pilesLen[i]) * sizeof(int));
			// Set the original array to its new values
			for (j=0; j<p.pilesLen[i]; j++) {
				p.piles[i][j] = temp[j];
			}
			// Free memory
			free(temp);
			free(p.piles[p.len-1-i]);
		}
		p.len -= p.len/2;
	}
	// Insert into *p in the same order
	for (i=0; i<p.pilesLen[0]; i++) {
		a[i] = p.piles[0][i];
	}

	// Free copied data
	clearPile(&p);

	return c;
}