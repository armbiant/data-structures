// Functions for managing standard arrays a bit more easily

#ifndef _STD_ARRAY_H_
#define _STD_ARRAY_H_

#include <stdint.h>

// Prints the list of items
void printList(int *p, int len);

// Shuffles the list randomly
void shuffle(int *p, int len, int seed);

// Check to see if the array is sorted
char checkList(int *p, int len, char print);

// Get the min/max index of an array
int arrayMin(int *p, int len);
int arrayMax(int *p, int len);

// Return an the average of an array's values, rounded down.
uint64_t arrayAverage(uint64_t *p, int len);

// Generate different types of arrays
int* generateAscendingArray(int* p, int len);
int* generateRandomArray(int* p, int len);
int* generateRepeatingArray(int* p, int len);

#endif // _STD_ARRAY_H_