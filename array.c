/* ==========================================================================
	    _
	   / \   _ __ _ __ __ _ _   _   ___
	  / _ \ | '__| '__/ _` | | | | / __|
	 / ___ \| |  | | | (_| | |_| || (__
	/_/   \_|_|  |_|  \__,_|\__, (_\___|
	                        |___/

	Functions for managing standard arrays

 ========================================================================== */

// Functions for managing standard arrays

#include "array.h"

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

// Print an array
void printList(int *p, int len) {
	int l = sizeof(p) / sizeof(p[0]);
	printf("[");
	for (int i=0; i<len; i++) {
		if (i == len-1) {
			printf("%i", p[i]);
		} else {
			printf("%i, ", p[i]);
		}
	}
	printf("]\n");
}

// Get the average (mean) of an array
uint64_t arrayAverage(uint64_t *p, int len) {
	uint64_t sum = 0;
	for (int i=0; i<len; i++) {
		sum += p[i];
	}
	return sum/len;
}

// Shuffle an array
void shuffle(int *p, int len, int seed) {
	int i;		// Loop iterator
	int s;		// Element with which to swap

	// Get time in ms
	struct timeval current_time;
	gettimeofday(&current_time, NULL);

	// Generate a random seed
	srand ((unsigned)(current_time.tv_sec+current_time.tv_usec+seed));

	// Loop through elements
	for (i=0; i<len-1; i++) {
		s = rand() % (len-i);
		//printf("i=%i, switching %i with %i\n",i,i,i+s);
		if(s){
			p[i] ^= p[i+s] ^= p[i] ^= p[i+s];
		}
	}
}

// Returns the index of the smallest value
int arrayMin(int *p, int len) {
	int min = 0;
	for (int i=1; i<len; i++) {
		min = p[i] < p[min] ? i : min;
	}
	return min;
}

// Returns the index of the largest value
int arrayMax(int *p, int len) {
	int max = 0;
	for (int i=1; i<len; i++) {
		max = p[i] > p[max] ? i : max;
	}
	return max;
}

// Check to see if a list is sorted
char checkList(int *p, int len, char print) {
	for (int i=1; i<len; i++) {
		if (p[i] < p[i-1]){
			if (print) {
				printf("Failed check at position %i, (%i<%i)\n", i-1, p[i], p[i-1]);
			}
			return 0;
		}
	}
	if (print) {
		printf("List is in order! :)\n");
	}
	return 1;
}

// Generate arrays

int* generateAscendingArray(int* p, int len) {
	p = realloc(p, len * sizeof(int));
	for (int i=0; i<len; i++) {
		p[i] = i;
	}
	return p;
}

int* generateRepeatingArray(int* p, int len) {
	// Set divisor to a positive integer
	int divisor = sqrt(len);
	p = realloc(p, len * sizeof(int));
	for (int i=0; i<len; i++) {
		p[i] = i/divisor;
	}
	return p;
}

int* generateRandomArray(int* p, int len) {
	static uint32_t seed = 0;
	// Get time in ms
	struct timeval current_time;
	gettimeofday(&current_time, NULL);

	// Generate a random seed
	srand ((unsigned)(current_time.tv_sec+current_time.tv_usec+seed));

	// Generate the array
	p = realloc(p, len * sizeof(int));
	for (int i=0; i<len; i++) {
		p[i] = rand() % (len-1);
	}

	// Increase seed and return the pointer to the new array location
	seed++;
	return p;
}