/* ==========================================================================
	 _____     _
	|_   _| __(_) ___   ___
	  | || '__| |/ _ \ / __|
	  | || |  | |  __/| (__
	  |_||_|  |_|\___(_)___|

	A trie is a data structure that works like a binary tree but instead of
	having two child nodes, it has any number n nodes. This means that it
	can't use comparators efficiently, but instead it allows for a fast
	assignment of sub-nodes via calculation of powers and logarithms.

 ========================================================================== */

// Functions for managing tries

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "trie.h"

// Initialise a node on the trie structure
void trieInit(trie* t, char size, char offset) {
	// Set the values of the trie's attributes
	t->value = 0;
	t->size = size;
	t->offset = offset;
	// Generate an array of trie pointers and set them all to zero
	t->trie = calloc(size, sizeof(trie*));
}
// Print out all trie elements in order
void printTrieRecursive(trie* t, int level) {
	int i, j;
	for (i=0; i<t->size; i++) {
		if (t->trie[i]) {
			// Do it recursively
			for (j=0; j<level; j++) {
				printf("\t");
			}
			printf("%i:%i\n", i, t->value);
			printTrieRecursive(t->trie[i], level+1);
		} else {
			// End here
		}
	}
}
void printTrie(trie* t) {
	printf("Trie size: %i, offset: %i\n", t->size, t->offset);
	printTrieRecursive(t, 0);
}

int trieInsertRecursive(trie* t, int value, int digits, char level) {
	if (digits+1 == level) {
		return(0);
	}
	int power = (int)pow(t->size, digits-level);
	int digit = ((value % (power * t->size))/power);
	trie* next = t->trie[digit];
	
	//printf("Value: %i, Power: %i, Digit: %i, Next:%p\n", value, power, digit, next);
	if (next) {
		// Sub-trie exists already
		if (digits == level) {
			//printf("D:%i, L:%i, V: %i\n", digits, level, value);
			t->trie[digit]->value++;
		}
		return trieInsertRecursive(t->trie[digit], value, digits, level+1);
	} else {
		// Create a new sub-trie by first allocating the struct
		t->trie[digit] = malloc(sizeof(trie));
		// Then initialise it by giving it some default values
		trieInit(t->trie[digit], t->size, t->offset);
		// Increase trie counter
		t->trie[digit]->value++;
		return trieInsertRecursive(t->trie[digit], value, digits, level+1);
	}
	return 0;
}

// Helper function
int trieInsert(trie* t, int value, int digits) {
	return trieInsertRecursive(t, value, digits, 0);
}

int trieToArrayRecursive(trie* t, int* p, int index, int maxDepth, int parentValue, char level) {
	int i, j;
	for (i=0; i<t->size; i++) {
		if (t->trie[i]) {
			// Check if we're on the final level
			if (level == maxDepth) {
				// Add to array for as many times as we have $value
				for (j=0; j<t->trie[i]->value; j++){
					p[index++] = parentValue + i*(int)pow(t->size, maxDepth-level);
				}
			} else {
				// Perform recursively
				index = trieToArrayRecursive(
					t->trie[i],	// Next sub-trie
					p,			// Pass the same array pointer
					index,		// Pass the same index
					maxDepth,	// Pass the same max depth
					// Increase parent value by current sub-trie x magnitude
					parentValue + (int)pow(t->size, maxDepth-level) * i,
					level+1		// Increase level by one
				);
			}
		}
	}
	// Return index to parent so that it can be sent to its sibling recursions
	return (index);
}
void trieToArray(trie* t, int* p, int maxDepth) {
	trieToArrayRecursive(t, p, 0, maxDepth, 0, 0);
}

// Use this to free the contents of the trie
void trieClear(trie* t) {
	int i, j;
	for (i=0; i<t->size; i++) {
		if (t->trie[i]) {
			trieClear(t->trie[i]);
			free(t->trie[i]);
		}
	}
	free(t->trie);
}
// Use this to also free the trie root
void trieDelete(trie* t) {
	trieClear(t);
	free(t);
}