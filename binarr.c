/* ==========================================================================
	 ____  _
	| __ )(_)_ __   __ _ _ __ _ __ ___ 
	|  _ \| | '_ \ / _` | '__| '__/ __|
	| |_) | | | | | (_| | |  | |_| (__ 
	|____/|_|_| |_|\__,_|_|  |_(_)\___|

	This is a reusable data structure for binary arrays. It is an array of
	chars whose individual bits can be switched on or off as needed. It is
	also known as Bit Fields, but I like to call them binary arrays, or
	binarr for short.

 ========================================================================== */

// Functions for managing binary arrays

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "binarr.h"

// Initialise a binary array to be all zeros
void binarrInit (binarr* b, int len) {
	b->len = len;
	b->p = calloc(len, sizeof(unsigned char));
}

// Set a bit to 1
void binarrSet (binarr* b, int pos, char val) {
	int byte = pos / 8;
	char bit = pos % 8;
	if (val) {
		// Set bit to 1
		b->p[byte] = b->p[byte] | (BINARR_1 << bit);
	} else {
		// Set bit to 0
		b->p[byte] = b->p[byte] & ~(BINARR_1 << bit);
	}
}

// Change the value of a bit
void binarrFlip (binarr* b, int pos) {
	int byte = pos / 8;
	char bit = pos % 8;
	// Flip it
	b->p[byte] = b->p[byte] ^ (BINARR_1 << bit);
}

// Return the value of a bit
int binarrGet (binarr* b, int pos) {
	int byte = pos / 8;
	char bit = pos % 8;
	return (int)b->p[byte] & (BINARR_1 << bit)?1:0;
}

// Print out a whole binary array
void binarrPrint (binarr* b) {
	int byte, bit;
	// Loop through bytes
	for (byte=b->len/8; byte>=0; byte--) {
		// Loop through bits
		for (bit=7; bit>=0; bit--) {
			printf("%i",b->p[byte] & (BINARR_1 << bit)?1:0);
		}
		printf(" ");
	}
	printf("\n");
}

// Print each byte onto multiple rows
void binarr2dPrint (binarr* b, int len) {
	for (int i=0; i<len; i++) {
		binarrPrint(&b[i]);
	}
}

// Perform a byte shift on the entire array
void binarrShift (binarr* b, int shifts) {
	int i;							// Loop iterator
	binarr carry;					// Temp binary array as a placeholder to avoid overwrite issues
	const char offset = shifts/8;	// How many bytes the shift is being performed on

	// Initi the carry array
	binarrInit(&carry, b->len);

	// Check if the shift is being done to the left or to the right
	if (shifts < 0) {
		// Shift it leftwards
		for (i=0; i<b->len/8-offset+1; i++) {
			// Shift the 'carry over' bits
			if (i-offset+1 < b->len)
				carry.p[i-offset+1] |= b->p[i] >> 8-(shifts%8)*-1;
			// Shift simple bits
			carry.p[i-offset] |= b->p[i] << (shifts%8)*-1;
		}
	} else {
		// Shift it rightwards
		for (i=b->len/8; i>=0+offset; i--) {
			// Shift the 'carry over' bits
			if (i-offset-1 >= 0)
				carry.p[i-offset-1] |= b->p[i] << 8-(shifts%8);
			
			// Shift simple bits
			carry.p[i-offset] |= b->p[i] >> shifts%8;
		}
	}
	// Copy from the carry array to the original one
	memcpy(b->p, carry.p, b->len/8*sizeof(char)+1);
	// Free memory from temp
	binarrFree(&carry);
}

// Free the memory
void binarrFree (binarr* b) {
	free(b->p);
}